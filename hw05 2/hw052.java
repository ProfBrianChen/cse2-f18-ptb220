import java.util.Scanner; //imports the scanner

public class Hw05 {

public static void main(String[] args){

    int numberOfTimes = 0; //number of times initially set to 0
    int card1 = 0; //card 1 value is initially 0
    int card2 = 0; //card 2 value is initiially 0
    int card3= 0; //card 3 value is initially 0
    int card4 = 0; //card 4 value is initially 0
    int card5 = 0; //card 5 value is initially 0
    int suit = 0; //suit value is initially 0
    int fourOfAKind = 0; //how many times 4 of a kind occurs is initially 0 
    int threeOfAKind = 0; //how many times 3 of a kind occurs is initially 0
    int twoPair = 0; //two pair is initially 0 
    int onePair = 0; // one pair is initially 0

    Scanner myScanner = new Scanner(System.in); //scanner for how many hands

    System.out.println("How many hands do you want: ");

        boolean isNumBool;
  
        do {
          isNumBool = myScanner.hasNextInt();
          numberOfTimes = myScanner.nextInt();
          if (isNumBool == false) {
            System.out.println("Incorrect Input");
          }
        } while (isNumBool == false);      
  /*
  while(myScanner.hasNextInt() == false){ //if not int, then asks for a valid int
        numberOfTimes = myScanner.nextInt();
        System.out.println("Not a valid number of hands. Please enter a valid number of hands.");
        }
  /*
        while(true) {
          
          if ((numberOfTimes = myScanner.hasNextInt()) == true) {
            break;
          }
          else {
            continue;
          }          
        }
  */
        int numTimes = numberOfTimes;
        
        while(numberOfTimes > 0){ //while loop for how many hands


        card1 = (int)(Math.random() * 52 + 1); //random number generator from 1-52 for cards 1 through 5
        card2 = (int)(Math.random() * 52 + 1);
        card3 = (int)(Math.random() * 52 + 1);
        card4 = (int)(Math.random() * 52 + 1);
        card5 = (int)(Math.random() * 52 + 1);

        int checkSuit = 0; // there are 4 suits so initially 0

        while(checkSuit < 4){ //while loop to check suits of cards

        if(checkSuit * 13 < card1 << checkSuit * 13 + 14){ //the suits of the cards are known depending on what numbers they fall between

        suit = suit + 1; //if checkSuit = 1 then suit is diamonds for 1 card
    }
        if(checkSuit * 13 < card2 << checkSuit * 13 + 14){

        suit = suit + 1;
    }
        if(checkSuit * 13 < card3 << checkSuit * 13 + 14){
        //the card will be a diamond
        suit = suit + 1;
    }
        if(checkSuit * 13 < card4 << checkSuit * 13 + 14){

        suit = suit + 1;
    }

        if(checkSuit * 13 < card5 << checkSuit * 13 + 14){

        suit = suit + 1;
    }

        checkSuit = checkSuit + 1;

        if(suit == 4){ //number of suits = 4 then there are four of a kind
        fourOfAKind = fourOfAKind + 1;
    }
        if(suit == 3){ //number of suits = 3 then there are three of a kind
        threeOfAKind = threeOfAKind + 1;
    }
        if (suit == 2){ //number of suits = 2 then two pair
        twoPair = twoPair + 1;
    }
        if (suit == 1){ //number of suits = 1 then one pair
        onePair = onePair + 1;
          break;
    }
    }
      numberOfTimes--;
    }
          System.out.println("The number of loops is: " + numberOfTimes);
          System.out.println("The probability of Four-of-a-kind is: " + fourOfAKind / numTimes);
          System.out.println("The probability of Three-of-a-kind is: " + threeOfAKind / numTimes);
          System.out.println("The probability of Two-pair is: " + twoPair / numTimes);
          System.out.println("The probability of One-pair is: " + onePair / numTimes);

   }
}
