//This homework gives you practice in manipulating arrays and in writing methods that have array parameters.

import java.util.Scanner; //import scanner
public class hw08{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); //scanner
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //rank
String[] cards = new String[52]; //array of cards
String[] hand = new String[5]; //hand
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); //array of cards
} 
System.out.println();
printArray(cards); //prints cards
shuffle(cards); //shuffles cards
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); //get a hand
   printArray(hand);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  }
  public static void printArray(String[] cards) { //prints cards
    for (int i=0; i < cards.length; i++){
     System.out.print(cards[i]+" "); 
    }
  }
  public static void shuffle(String[] cards) { //method for shuffling cards
   int i=0;
   int a=0;
   String x="";
    while (i<50){ //prints an array of random cards (shuffle)
     a = (int)(Math.random()*51 + 1); //prints random integer from 1-52
     x = cards[0]; //x is cards array
     cards[0] = cards[a];
     cards[a] = x;
      i++;
    }
  }
 public static String[] getHand(String[] cards, int index, int numCards) { //to get a hand
   int b = 0;
   String[] hand = new String[numCards]; 
   while (b < numCards) {
     if (index < 0){
       shuffle(cards);
       index = 51;
     }
    hand[b]=cards[index];
     b++;
       index--;
     } 
    return hand; //returns a hand
   }
    
 }
    

