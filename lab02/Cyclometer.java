//Parker Bass Lab02 CSE2 Section 311
//For two trips, given time and rotation count, your program should print the number of minutes of each trip, the number of counts for each trip, the distance of each trip, and the distance for the two trips combined
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
    //input data
      int secsTrip1=480;  //time of trip 1
       	int secsTrip2=3220;  //time of trip 2
		int countsTrip1=1561;  //counts of trip 1
		int countsTrip2=9037; //counts of trip 2
      // our intermediate variables and output data
      double wheelDiameter=27.0; 
      double PI=3.14159; 
      double feetPerMile=5280;
      double inchesPerFoot=12;
      double secondsPerMinute=60;  //Needed constants for the data 
	double distanceTrip1;
      double distanceTrip2;
      double totalDistance;  //Output data we are looking for
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts."); //Prints how long trip 1 took in minutes and how many counts there were
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); //Prints how long trip 2 took in minutes and how many counts there were
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
      distanceTrip1=countsTrip1*wheelDiameter*PI; //Gives distance of Trip 1 in inches
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance of Trip 1 in miles
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Gives distance of Trip 2 in inches
 totalDistance=distanceTrip1+distanceTrip2; //Calculation for total distance
      //Print out the output data.
           System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
    }
}

	



