import java.util.Scanner; //imports a scanner
public class EncryptedX {
  //main method
  public static void main(String [] args) {
    Scanner myScanner = new Scanner( System.in ); //scanner
    System.out.println ("Input an integer between 0 and 100: "); //asks user to input integer between 0 and 100
    int boxWidth = 0; //initializes boxWidth
    int boxHeight = 0; //initializes boxHeight
     boolean isNumBool; //boolean for scanner
      
        do {
          isNumBool = myScanner.hasNextInt(); //first step of scanner to check boxWidth is valid
          boxWidth = myScanner.nextInt(); //scanner for boxWidth
          if (isNumBool == false || boxWidth < 0 || boxWidth > 100) { //checks to see if its a valid integer betweeen 0 and 100
            System.out.println("Incorrect Input. Input an integer between 0 and 100."); //prints if incorrect
          }
        } while (isNumBool == false || boxWidth < 0 || boxWidth > 100); //checks for valid input and stops loop if input is correct    
    
    boxHeight = boxWidth; //square so boxheight = boxwidth
    int space1 = -1; //line 1 starts with a space
    int space2 = boxWidth; // = boxwidth because one line goes up and down (diagonally, end to end)
    for(int i = 0; i < boxHeight; i++){ //iterate through height
      space1++;
      space2--;
      for(int j = 0; j < boxWidth; j++){ //iterate through wideth
       if (j == space1){
         System.out.print(" "); //prints a space for the first space
         continue;
         
       }
        if(j == space2){
          System.out.print(" "); //prints for second space
          if(space2 == boxWidth - 1){ // sometimes space 2 is end of line, but if space2 is not the end of line just print a space
            System.out.println();
          }
          continue;
        }
        System.out.print("*"); //prints asterisks otherwise
        
        if (j == boxWidth - 1){
          
         System.out.println(); //prints a new line character
       }
      }
      
    }
  }
}


 