//Write a program that asks the user for doubles that represent the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average
import java.util.Scanner;
public class Convert{
  
  public static void main(String args[]){
   Scanner myScanner = new Scanner( System.in ); //scanner
    System.out.print("Enter the affected area in acres: "); //prints and prompts to enter affected area
    double affectedAreaInAcres = myScanner.nextDouble(); //enter affected area in acres
    System.out.print("Enter the rainfall in the affected area: "); //enter the rainfall in the affected area
    double a = Math.pow( 10, -15 );
    double b = Math.pow( 10, 13 );
    double rainfallInAffectedArea = myScanner.nextDouble(); //enter the rainfall in the affected area
    double totalRainfall = (rainfallInAffectedArea * affectedAreaInAcres * 3.93 * a * 1.77 * b); //converts rainfall to cubic miles
    System.out.println ("Total rainfall in cubic miles: " + totalRainfall); //prints total rainfall

    
  }
}

    