//Write a program that prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid

import java.util.Scanner;
public class Pyramid{
  
  public static void main(String args[]){
Scanner myScanner = new Scanner( System.in ); //scanner
    System.out.print("Enter the input length of the square side of the pyramid: "); //print and enter the length of the side of the pyramid
    double squareSideOfPyramidLength = myScanner.nextDouble(); //scans  for length of pyramid side
    System.out.print("Enter the height of the pyramid: "); //print and enter height of pyramid
    double heightOfPyramid = myScanner.nextDouble(); //scans for height of pyramid
    double volume = ((squareSideOfPyramidLength * squareSideOfPyramidLength * heightOfPyramid)/3); //calculates volume of pyramid
    System.out.println ("The volume of the pyramid is: " + volume); // prints volume of pyramid
  }
}
