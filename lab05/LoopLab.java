import java.util.Scanner;
public class LoopLab {
  //main method
  public static void main(String [] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    int courseNumber = 0;
    String departmentName = "";
    int numberPerWeek = 0;
    double classTime = 0.0;
    String instructorName = "";
    int numberOfStudents = 0;
    
    System.out.println("Enter the course number: ");
    
    while(myScanner.hasNextInt() == false){
      
      myScanner.next();
      System.out.println("Not a valid course number. Enter a valid course number.");
      
    }
   courseNumber = myScanner.nextInt();
    
    System.out.println("Enter the Department Name: ");
    
    while(myScanner.hasNext() == false){
      
      myScanner.next();
      System.out.println("Not a valid department name. Enter a valid department name.");
    }
    departmentName = myScanner.next();
    System.out.println("Enter the number of times the class meets per week: ");
    
    while(myScanner.hasNextInt() == false){
      myScanner.next();
      System.out.println("Not a valid integer. Enter the correct number of times the class meets per week.");
    }
    numberPerWeek = myScanner.nextInt();
    
    System.out.println("Enter the time the class starts: ");
    
    while(myScanner.hasNextDouble() == false){
      myScanner.next();
      System.out.println("Not a valid class time. Enter the correct class time.");
    }
    classTime = myScanner.nextDouble();
    System.out.println("Enter the instructor name: ");
    
    while(myScanner.hasNext() == false){
      myScanner.next();
      System.out.println("Not a valid instructor name. Enter the correct instructor name.");
    }
    instructorName = myScanner.next();
    System.out.println("Enter the number of students in the class: ");
    
    while(myScanner.hasNextInt() == false){
      myScanner.next();
      System.out.println("Not a valid number of students. Enter the correct number of students.");
    }
  }
}

    