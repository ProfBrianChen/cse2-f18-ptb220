//Write a code for Craps using switch statements
import java.util.Scanner; //imports a scanner

public class CrapsSwitch{
  
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in );
    System.out.println ("Do you want randomly cast dice? (Type Yes or No)"); //prints "Do you want randomly cast dice
    String input = myScanner.next(); //scans for yes or no input
    int die1; //initializes die 1 value
    int die2; //initalizes die 2 value

    if (input.equals("Yes")){ //if input is "yes" inputs random roll for both die

      die1 = (int)(Math.random()*6) + 1; //makes die 1 a random number from 1-6
      die2 = (int)(Math.random()*6) + 1; //makes die 2 a random number from 1-6
    }
    else if (input.equals ("No")){ // if input is "no" asks for 2 die values
      System.out.println("Enter two die values"); //Prints "enter two die values"
      int rangeChecker = myScanner.nextInt(); //scans for input
      if (rangeChecker > 6 || rangeChecker < 1){ //checks to make sure roll is between 1 and 6
        System.out.println("Must Enter an Integer Between 1 and 6"); //if not between 1 and 6, prints "must enter an entiger between 1 and 6
      }
      die1 = rangeChecker; //die1 = rangeChecker (input value)
      rangeChecker = myScanner.nextInt(); //scans for value
      if (rangeChecker > 6 || rangeChecker < 1){
        System.out.println("Must enter an Integer Between 1-6"); //prints if not within range
      }
      die2 = rangeChecker; //die2 = rangeChecker (input value)
    }
    else { //if not yes or no, defaults to 1
      System.out.println("Incorrect input, die defaulted to 1"); //prints if not yes or no
      die1 = 1; //defaults die 1 to 1
      die2 = 1; //defaults die 2 to 1
    }
    System.out.println(die1 + " " +die2);
   switch (die1){
        case 1: // check if var is 1
          switch (die2) {
            case 1:  // die1 == 6 && die2 == 1
                System.out.println("1 and 1, Snakeyes"); //prints if die1 is 1 and die2 is 1
                break; //puts a break in the code
            case 2: // die1 == 1 && die2 == 2
                System.out.println("1 and 2, Ace Deuce");
                break;
            case 3:
                 System.out.println("1 and 3, Easy Four");
            case 4:
                 System.out.println("1 and 4, Fever Five");
      case 5:  
         System.out.println("1 and 5, Easy Six");
      case 6:
         System.out.println("1 and 6, Seven Out");
      break;
      }
     break;
   case 2:
     switch (die2){
       case 1:
         System.out.println("2 and 1, Ace Deuce");
       case 2:
         System.out.println("2 and 2, Hard Four");
       case 3:
         System.out.println("2 and 3, Fever Five");
       case 4:
         System.out.println("2 and 4, Easy Six");
       case 5:
         System.out.println("2 and 5, Seven Out");
       case 6:
         System.out.println("2 and 6, Easy Eight");
         break;
     }
     break;
       case 3:
         switch (die2){
           case 1:
             System.out.println("3 and 1, Easy Four");
           case 2:
             System.out.println("3 and 2, Fever Five");
           case 3:
             System.out.println("3 and 3, Hard Six");
           case 4:
             System.out.println("3 and 4, Seven Out");
           case 5:
             System.out.println("3 and 5, Easy Eight");
           case 6:
             System.out.println("3 and 6, Nine");
             break;
         }
        break;
       case 4:
         switch (die2){
           case 1:
             System.out.println("4 and 1, Fever Five");
           case 2:
             System.out.println("4 and 2, Easy Six");
           case 3:
             System.out.println("4 and 3, Seven Out");
           case 4:
             System.out.println("4 and 4, Hard Eight");
           case 5:
             System.out.println("4 and 5, Nine");
           case 6:
             System.out.println("4 and 6, Easy Ten");
             break;
         }
    break;
         case 5:
           switch(die2){
             case 1: 
               System.out.println("5 and 1, Easy Six");
             case 2:
               System.out.println("5 and 2, Seven Out");
             case 3:
               System.out.println("5 and 3, Easy Eight");
             case 4:
               System.out.println("5 and 4, Nine");
             case 5:
               System.out.println("5 and 5, Hard Ten");
             case 6:
               System.out.println("5 and 6, Yo-leven");
               break;
           }
       break;
         case 6:
           switch (die2){
             case 1:
               System.out.println("6 and 1, Seven Out");
             case 2:
               System.out.println("6 and 2, Easy Eight");
             case 3:
               System.out.println("6 and 3, Nine");
             case 4:
               System.out.println("6 and 4, Easy Ten");
             case 5:
               System.out.println("6 and 5, Yo-leven");
             case 6:
               System.out.println("6 and 6, Boxcars");
               break;
           }
   }
  }
}

       