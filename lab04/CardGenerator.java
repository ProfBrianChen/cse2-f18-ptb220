//write a code that picks a random card
import java.lang.Math;
public class CardGenerator{ //main method required for every java program
  public static void main(String[] args){ //Our input data
    int cardValue; //Variable to be used for generating the number
    int valueReduced; //changes value to 1-13
    int max = 52; //Max card number
    int min = 1; //min card number
    int range = max - min + 1; //range of card numbers
    String suit; //String variable for suit
    String identity = "test"; //String variable for identity
    cardValue = (int) (Math.random() * range) + min; //generates random number within range 1 to 52 inclusive
    if (cardValue < 14){ //Cheks if number is less than 14
      suit = "Diamonds";
      valueReduced = cardValue;
    }
    else if(cardValue < 27){ //if not <14, checks if in next set
      suit = "Clubs";
      valueReduced = cardValue - 13; //Sets reduced value
    }
    else if(cardValue < 40){ //if not in first two sets, checks if in third set
      suit = "Hearts";
      valueReduced = cardValue - 26; //Sets reduced value
    }
    else { //if not in first three sets, has to be in fourth set
      suit = "Spades";
      valueReduced = cardValue - 39; //Sets reduced value
    }
    switch (valueReduced){ //tests each case for card identity
    case 1:
      identity = "Ace"; 
    break;
    case 2:
      identity = "2";
    break;
    case 3:
      identity = "3";
    break;
    case 4:
      identity = "4";
    break;
    case 5:
      identity = "5";
    break;
    case 6:
      identity = "6";
    break;
    case 7:
      identity = "7";
    break;
    case 8:
      identity = "8";
    break;
    case 9:
      identity = "9";
    break;
    case 10:
      identity = "10";
    break;
    case 11:
      identity = "Jack";
    break;
    case 12:
      identity = "Queen";
    break;
    case 13:
      identity = "King";
    break;
      }
    System.out.println("You picked the " + identity + " of " + suit); //prints out the final statement
    
 }
  }
