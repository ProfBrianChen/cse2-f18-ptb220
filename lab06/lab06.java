import java.util.Scanner;
public class lab06 {
  //main method
  public static void main(String [] args) {
    
    Scanner myScanner = new Scanner(System.in);
    
    
    System.out.println("Enter an integer between 1 and 10: ");
    
   System.out.println("How many rows you want in this pattern?"); //asks the user how many row they want
         
        int numRows = myScanner.nextInt();
         
         System.out.println("Pattern D: ");
    
        for (int i = numRows; i >= 1; i--) //for a pyramid that goes in descending order
        {
            for (int j = i; j >= 1; j--)
            {
                System.out.print(j+" "); //prints the pyramid
            }
             
            System.out.println(); //prints the pyramid
        }
         
        //Close the pyramid
         
        myScanner.close();
    
    
  }
}
