public class Arithmetic{
  
  public static void main(String args[]){
    //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;
 double totalCostOfPants = numPants * pantsPrice;   //total cost of pants
    double totalCostOfShirts = numShirts * shirtPrice; //total cost of shirts
    double totalCostOfBelts = numBelts * beltCost; //total cost of belts
    double pantsTax = totalCostOfPants * paSalesTax; //sales tax for pants
    double shirtTax = totalCostOfShirts * paSalesTax; //sales tax for shirts
    double beltTax = totalCostOfBelts * paSalesTax; //sales tax for belts
    double pantsTaxRound = ( (double) ( (int) (pantsTax * 100)) / 100); //rounds pants tax to 2 decimal places
    double shirtTaxRound = ( (double) ( (int) (shirtTax * 100)) / 100); //rounds shirt tax to 2 decimal places
    double beltTaxRound = ( (double) ( (int) (beltTax * 100)) / 100); //rounds belt tax to 2 decimal places
    double totalPurchases = (totalCostOfPants + totalCostOfShirts +totalCostOfBelts); //total cost of purchases before tax
   double totalSalesTax = (pantsTaxRound + shirtTaxRound + pantsTaxRound); //total sales tax
   double totalPaid = totalPurchases + totalSalesTax; //total paid
   System.out.println("Total Cost of Pants: " + totalCostOfPants); //prints total cost of pants
   System.out.println("Total Cost of Shirts: " + totalCostOfShirts); //prints total cost of shirts
   System.out.println("Total Cost of Belts: " + totalCostOfBelts); //prints total cost of belts
   System.out.println("Tax Charged on Pants: " + pantsTaxRound); //prints tax charged on pants rounded
   System.out.println("Tax Charged on Shirts: " + shirtTaxRound); //prints tax charged on shirts rounded
   System.out.println("Tax Charged on Belts: " + beltTaxRound); //prints tax charged on belts rounded
   System.out.println("Total Cost of Purchase Before Tax: " + totalPurchases); //prints total cost of purchase before tax
   System.out.println("Total Sales Tax: " + totalSalesTax); //prints total sales tax
   System.out.println("Total Paid: " + totalPaid); //prints total paid on everything
  }
}

     
    