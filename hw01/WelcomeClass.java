public class WelcomeClass {
  
  public static void main(String args[]){
    ///prints Welcome to terminal window
    System.out.println( "-----------");
    System.out.println ("| WELCOME |");
    System.out.println (" -----------");
    System.out.println ("  ^  ^  ^  ^  ^  ^");
    System.out.println (" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println ("<-P--T--B--2--2--0->");
    System.out.println (" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println ("  v  v  v  v  v  v");
  }
}
/*
My name is Parker Bass. I am a Junior in ISE. My favorite classes are math and science classes, but I am very interested in learning more about coding.My
 */
 
 
